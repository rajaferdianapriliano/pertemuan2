Ext.define('pertemuan.store.Register', {
    extend: 'Ext.data.Store',
    storeId: 'data',
    alias: 'store.data',
    //autoLoad: true,
    autoSync: true,

    fields: [
        'id_user','username', 'password'
    ],
    proxy: {
        type: 'jsonp',
        api: {
            read: "http://localhost/MyApp_php/login.php"
            
        },
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});
