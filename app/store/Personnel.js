Ext.define('pertemuan.store.Personnel', {
    extend: 'Ext.data.Store',
    storeId: 'personnel',
    alias: 'store.personnel',
    //autoLoad: true,
    autoSync: true,

    fields: [
        'user_id','username', 'email', 'phone', 'song'
    ],
    proxy: {
        type: 'jsonp',
        api: {
            read: "http://localhost/MyApp_php/personnel.php",
            update: "http://localhost/MyApp_php/updatePersonnel.php",
            destroy: "http://localhost/MyApp_php/destroyPersonnel.php",
            create: "http://localhost/MyApp_php/createPersonnel.php"
            
        },
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    },
    listeners: {
        beforeload: function(store, operation, e0pts){
            this.getProxy().setExtraParams({
                user_id: -1
            });
        }
    }
});
