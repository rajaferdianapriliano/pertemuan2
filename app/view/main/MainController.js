/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('pertemuan.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',
    onItemSelected: function (sender, record) {
        Ext.getStore('personnel').filter('username', record.data.username)
        /*Ext.getStore('personnel').remove(record);
        alert("data sudah dihapus");*/
    },
    onConfirm: function (choice) {
        if (choice === 'yes') {
            localStorage.removeItem('LoggedIn');
            this.getView().destroy();
            Ext.getCmp('loginview').destroy();
            this.overlay = Ext.Viewport.add({
                        xtype: 'mylogin',
                        id: 'loginview',
                        floated: true,
                        //modal: true,
                        //hideOnMaskTap: true,
                        showAnimation: {
                            type: 'popIn',
                            duration: 250,
                            easing: 'ease-out'
                        },
                        hideAnimation: {
                            type: 'popOut',
                            duration: 250,
                            easing: 'ease-out'
                        },
                        centered: true,
                        width: "100%",//Ext.filterPlatform('ie10') ? '100%' : (Ext.os.deviceType == 'Phone') ? 260 : 400,
                        maxHeight: "100%",
                        header: {
                            title: 'Login Form'
                        }
            });
            Ext.getCmp('loginview').show();
        }
    },
    onClickButton: function(){
        Ext.Msg.confirm('Logout', 'Apakah anda ingin logout?','onConfirm', this);
    },

    onReadClicked: function (){
        Ext.getStore('personnel').load();
    },
    onSimpanPerubahan: function(){
        username = Ext.getCmp('myusername').getValue();
        email = Ext.getCmp('myemail').getValue();
        phone = Ext.getCmp('myphone').getValue();
        song = Ext.getCmp('mysong').getValue();
        store = Ext.getStore('personnel');
        record = Ext.getCmp('mydataview').getSelection();
        index = store.indexOf(record);
        record = store.getAt(index);
        store.beginUpdate();
        record.set('username',username);
        record.set('email',email);
        record.set('phone',phone);
        record.set('song',song);
        store.endUpdate();
        alert("Updating..");
    },
    onTambahData: function(){
        username = Ext.getCmp('myusername').getValue();
        email = Ext.getCmp('myemail').getValue();
        phone = Ext.getCmp('myphone').getValue();
        song = Ext.getCmp('mysong').getValue();
        store = Ext.getStore('personnel');
        store.beginUpdate();
        store.insert(0, {'username':username, 'email':email, 'phone':phone, 'song':song});
        store.endUpdate();
        alert("Inserting..");
    }
});
function onDeletePersonnel(user_id){
    record = Ext.getCmp('mydataview').getSelection();
    Ext.getStore('personnel').remove(record);
    alert(user_id);
};
function onUpdatePersonnel(user_id){
    record = Ext.getCmp('mydataview').getSelection();
    username = record.data.username;
    email = record.data.email;
    phone = record.data.phone;
    song = record.data.song;
    Ext.getCmp('myusername').setValue(username);
    Ext.getCmp('myemail').setValue(email);
    Ext.getCmp('myphone').setValue(phone);
    Ext.getCmp('mysong').setValue(song);
};