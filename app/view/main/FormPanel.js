Ext.define('pertemuan.view.forms.FormPanel', {
    extend: 'Ext.form.Panel',

    shadow: true,
    xtype: 'editform',
    id: 'editform',
    items: [
                {
                    xtype: 'textfield',
                    name: 'username',
                    id: 'myusername',
                    label: 'Username',
                    placeHolder: 'your name',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'emailfield',
                    name: 'email',
                    id: 'myemail',
                    label: 'Email',
                    placeHolder: 'me@sencha.com',
                    clearIcon: true
                },
                 {
                    xtype: 'textfield',
                    name: 'phone',
                    id: 'myphone',
                    label: 'Phone',
                    placeHolder: '0895332578',
                    autoCapitalize: true,
                    clearIcon: true
                },
                 {
                    xtype: 'textfield',
                    name: 'song',
                    id: 'mysong',
                    label: 'Song',
                    placeHolder: 'song',
                    autoCapitalize: true,
                    clearIcon: true
                },
                 {
                    xtype: 'button',
                    ui: 'action',
                    text: 'Simpan Perubahan',
                    handler: 'onSimpanPerubahan'
                },
                 {
                    xtype: 'button',
                    ui: 'confirm',
                    text: 'Tambah Data',
                    handler: 'onTambahData'
                }
    ]
});