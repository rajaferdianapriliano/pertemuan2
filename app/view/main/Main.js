/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 *
 * TODO - Replace the content of this view to suit the needs of your application.
 */
Ext.define('pertemuan.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',
    id: 'app-main',

    requires: [
        'Ext.MessageBox',
        'pertemuan.view.main.MainController',
        'pertemuan.view.main.MainModel',
        'pertemuan.view.form.user',
        'pertemuan.view.main.List',
        'pertemuan.view.setting.carausel',
        //'pertemuan.view.dataview.BasicDataView',
        //'pertemuan.view.dataview.DataView',
        'pertemuan.view.forms.FormPanel',
        'pertemuan.view.form.Login'

    ],

    controller: 'main',
    viewModel: 'main',

    defaults: {
        tab: {
            iconAlign: 'top'
        },
        styleHtmlContent: true
    },

    tabBarPosition: 'bottom',

    items: [
        {
            title: 'Users',
            iconCls: 'x-fa fa-user',
            layout: 'fit',
            items: [{
                xtype: 'user'
            }]
        }, {
            title: 'Pemutar Musik',
            iconCls: 'x-fa fa-cog',
            layout: 'fit',
            items: [{
                xtype: 'carausel'
            }]
        }, 
        {
            docked: 'top',
            xtype: 'toolbar',
            items: [
                {
                    xtype:'button',
                    text: 'Read',
                    ui: 'action',
                    scope: this,
                    listeners: {
                        tap: 'onReadClicked'
                    }
                },
                {
                    xtype:'button',
                    text: 'Logout',
                    ui: 'action',
                    scope: this,
                    listeners: {
                        tap: 'onClickButton'
                    }
                }
            ]
        },
        {
            title: 'Tampil Data',
            iconCls: 'x-fa fa-home',
            layout: 'fit',
            // The following grid shares a store with the classic version's grid as well!
            items: [{
                xtype: 'panel',
                layout: 'hbox',
                items: [{
                    xtype: 'mainlist',
                    flex: 2
                }/*{
                    xtype: 'specific',
                    flex: 1
                },*/]
            }]
        }
    ]    
});
