Ext.define('pertemuan.view.dataview.DataView', {
    extend: 'Ext.Container',
    xtype: 'specific',
    requires: [
        'Ext.dataview.plugin.ItemTip',
        'Ext.plugin.Responsive',
        'pertemuan.store.Personnel'
    ],

    
    layout: 'fit',
    cls: 'ks-basic demo-solid-background',
    shadow: true,
    items: [{
        xtype: 'dataview',
        scrollable: 'y',
        id: 'mydataview',
        cls: 'dataview-basic',
        itemTpl: 'Username: {username}<br>Email: {email}<br>Phone: {phone}<br>Song: {song}<br><button type=button onclick="onDeletePersonnel({user_id})">Hapus</button><br><button type=button onclick="onUpdatePersonnel({user_id})">Edit</button><hr>',
        store: 'personnel',
        /*plugins: {
            type: 'dataviewtip',
            align: 'l-r?',
            plugins: 'responsive',
            
            // On small form factor, display below.
            responsiveConfig: {
                "width < 600": {
                    align: 'tl-bl?'
                }
            },
            width: 600,
            minWidth: 300,
            //delegate: '.img',
            allowOver: true,
            anchor: true,
            bind: '{record}',
            tpl: '<table style="border-spacing:3px;border-collapse:separate">' + 
                    '<tr><td>Username: </td><td>{username}</td></tr>' +
                    '<tr><td>Email:</td><td>{email}</td></tr>' +
                    '<tr><td>Phone:</td><td>{phone}</td></tr>' +
                    '<tr><td>Song:</td><td>{song}</td></tr>' + 
                    '<tr><td vAlign="top">Bio:</td><td><div style="max-height:100px;overflow:auto;padding:1px">{bio}</div></td></tr>'
        }*/
    }]
});