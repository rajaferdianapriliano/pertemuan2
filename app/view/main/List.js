/**
 * This view is an example list of people.
 */
Ext.define('pertemuan.view.main.List', {
    extend: 'Ext.grid.Grid',
    xtype: 'mainlist',

    requires: [
        'pertemuan.store.Personnel'
    ],

    title: 'Personnel',
    requires: [
        'Ext.grid.plugin.Editable',
    ],
    plugins: [{
        type: 'grideditable'
    }],
    /*store: {
        type: 'personnel'
    },*/

    bind: '{personnel}',

    viewModel: {
        stores: {
            personnel: {
                type: 'personnel'
            }
        }
    },
    columns: [
        { text: 'username', dataIndex: 'username', width: 170, editable: true },
        { text: 'email', dataIndex: 'email', width: 230, editable: true},
        { text: 'phone', dataIndex: 'phone', width: 170, editable: true },
        { text: 'song', dataIndex: 'song', width: 170 },
    ],

    listeners: {
        select: 'onItemSelected'
    }
});
