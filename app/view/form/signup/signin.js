/**
 * Demonstrates a tabbed form panel. This uses a tab panel with 3 tabs - Basic, Sliders and Toolbars - each of which is
 * defined below.
 *
 * See this in action at http://dev.sencha.com/deploy/sencha-touch-2-b3/examples/kitchensink/index.html#demo/forms
 */
Ext.define('pertemuan.view.form.signup.signin', {
    extend: 'Ext.form.Panel',

    requires: [
        'Ext.field.Email',
        'Ext.field.Password',
    ],
    shadow: true,
    cls: 'demo-solid-background',
    xtype: 'login',
    id: 'login',
    items: [
        {
            xtype: 'fieldset',
            id: 'fieldsetlogin',
            title: 'Personal Info',
            instructions: 'Please enter the information above.',
            defaults: {
                labelWidth: '35%'
            },
            items: [
                {
                    xtype: 'emailfield',
                    name: 'email',
                    label: 'Email',
                    placeHolder: 'me@sencha.com',
                    clearIcon: true
                },
                {
                    xtype: 'passwordfield',
                    revealable: true,
                    name: 'password',
                    label: 'Password',
                    clearIcon: true
                },
            ]
        },
        {
            xtype: 'container',
            defaults: {
                xtype: 'button',
                style: 'margin: 1em',
                flex: 1
            },
            layout: {
                type: 'hbox'
            },
            items: [
                {
                    text: 'Sign Up',
                    ui: 'action',
                    scope: this,
                    hasDisabled: false,
                    handler: function (btn) {
                        var fieldset1 = Ext.getCmp('fieldset1'),
                            fieldset2 = Ext.getCmp('fieldset2');

                        if (btn.hasDisabled) {
                            fieldset1.enable();
                            fieldset2.enable();
                            btn.hasDisabled = false;
                            btn.setText('Disable fields');
                        } else {
                            fieldset1.disable();
                            fieldset2.disable();
                            btn.hasDisabled = true;
                            btn.setText('Enable fields');
                        }
                    }
                },
                {
                    text: 'Sign In',
                    ui: 'action',
                    handler: function () {
                        Ext.getCmp('basicform').reset();
                    }
                }
            ]
        }
    ]
});