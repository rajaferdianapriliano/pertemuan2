    Ext.define('pertemuan.view.form.Login', {
        extend: 'Ext.form.Panel',

        requires: [
            'Ext.form.FieldSet',
            'Ext.field.Password'
            
        ],
        shadow: true,
        cls: 'demo-solid-background',
        xtype: 'mylogin',
        id: 'mylogin',
        items: [
            {
                        xtype: 'textfield',
                        id: 'username',
                        label: 'Username',
                        required: true,
                        clearIcon: true
                    },
                    {
                        xtype: 'passwordfield',
                        revealable: true,
                        id : 'password',
                        label: 'Password',
                        clearIcon: true
                    },
                    {
                        xtype: 'button',
                        text: 'Login',
                        ui: 'round',
                        style:{
                            background: 'blue',
                            color: 'white'
                        },
                        handler:function(){
                           

                            var username = Ext.getCmp('username').getValue();
                                password = Ext.getCmp('password').getValue();

                                store = Ext.getStore('data');
                            var extraParams = {username: username, password: password};
                                store.getProxy().setExtraParams(extraParams);
                                store.load(function(me, records, successful, operation, eOpts){
                                    
                                    if (successful){
                                        localStorage.setItem("LoggedIn",true);
                                        Ext.getCmp('loginview').hide();
                                    }else{
                                        Ext.toast('Login Gagal');
                                    }
                                });
                        }
                    }
        ]
    });