/**
 * Demonstrates usage of the Ext.tab.Panel component with the tabBar docked to the bottom of the screen.
 * See also app/view/Tabs.js for an example with the tabBar docked to the top
 */
Ext.define('pertemuan.view.form.user', {
    extend: 'Ext.tab.Panel',
    requires: [
        'Ext.MessageBox',
        'pertemuan.view.form.signup.signin',
        'pertemuan.view.form.signup.signup',
        'pertemuan.view.forms.FormPanel'
    ],

    shadow: true,
    cls: 'demo-solid-background',
    xtype: 'user',
    id: 'user',
    activeTab: 0,
    tabBar: {
        layout: {
            pack: 'center',
            align: 'center'
        },
        docked: 'top',
        defaults: {
            iconAlign: 'top'
        }
    },
    defaults: {
        scrollable: true
    },
    items: [
        {
            title: 'Sign Up',
            cls: 'card',
            iconCls: 'x-fa fa-user',
            xtype: 'register'
        },
        {
            title: 'Sign In',
            cls: 'card',
            iconCls: 'x-fa fa-user',
            xtype: 'login'
        },
        {
            title: 'Form',
            cls: 'card',
            iconCls: 'x-fa fa-user',
            xtype: 'editform'   
        }
    ]
});