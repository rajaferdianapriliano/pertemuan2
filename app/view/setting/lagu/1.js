/**
 * Demonstrates usage of the Ext.Audio component
 */
Ext.define('pertemuan.view.setting.carausel.lagu.1', {
    extend: 'Ext.Container',
    xtype: 'musik1',
    id: 'musik1',
    requires: [
        'Ext.Audio'
    ],
    listeners: {
        hide: function () {
            try {
                var video = this.down('audio');
                video.fireEvent('hide');
            }
            catch (e) {
            }
        },
        show: function () {
            try {
                var video = this.down('audio');
                video.fireEvent('show');
            }
            catch (e) {
            }
        }
    },
    layout: Ext.os.is.Android ? {
        type: 'vbox',
        pack: 'center',
        align: 'center'
    } : 'fit',
    items: [
        {
            xtype: 'toolbar',
            docked: 'top',
            title: 'Ext.Audio'
        }
    ],
    items: Ext.os.is.Android ? [
        {
            xtype: 'audio',
            cls: 'myAudio',
            url: '../resources/1.mp3',
            loop: true,
            enableControls: false
        },
        {
            xtype: 'button',
            text: 'Play audio',
            margin: 20,
            handler: function () {
                var audio = this.getParent().down('audio');

                if (audio.isPlaying()) {
                    audio.pause();
                    this.setText('Play audio');
                } else {
                    audio.play();
                    this.setText('Pause audio');
                }
            }
        }
    ] : [
            {
                xtype: 'audio',
                cls: 'myAudio',
                url: '../resources/1.mp3',
                loop: true
            }
        ]
});