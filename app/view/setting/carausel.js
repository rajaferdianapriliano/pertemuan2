/**
 * Demonstrates the {@link Ext.Menu} component
 */
Ext.define('pertemuan.view.setting.carausel', {
    extend: 'Ext.Panel',
    requires: ['Ext.Menu'
    ],

    cls: 'demo-solid-background',
    xtype: 'carausel',
    id: 'carausel',
    shadow: true,
    padding: 20,
    scrollable: true,
    defaults: {
        xtype: 'button',
        cls: 'demobtn',
        margin: '10 0'
    },
    items: [
        {
            text: 'List Lagu',
            handler: function () {
                Ext.Viewport.toggleMenu('top');
            }
        },
        {
            iconCls: 'x-fa fa-undo ',
            text: 'Back',
            handler: function () {
                Ext.Viewport.toggleMenu('top');
            }
        },
    ],

    initialize: function () {

        Ext.Viewport.setMenu(this.getMenuCfg('top'), {
            side: 'top',
            reveal: true
        });
    },

    doDestroy: function () {
        Ext.Viewport.removeMenu('bottom');
        this.callParent();
    },

    getMenuCfg: function (side) {
        return {
            items: [
                {
                    text: 'Nanang Bebas Lagu Contoh',
                    scope: this,
                    handler: function () {
                        Ext.Viewport.hideMenu(side);
                    }
                },
                {
                    iconCls: 'x-fa fa-gear',
                    scope: this,
                    xtype: 'musik1'
                },
                {
                    xtype: 'button',
                    text: 'Armada - Buka Hatimu',
                    scope: this,
                    handler: function () {
                        Ext.Viewport.hideMenu(side);
                    }
                },
                {
                    iconCls: 'x-fa fa-pencil',
                    scope: this,
                    handler: function () {
                        Ext.Viewport.hideMenu(side);
                    },
                    xtype: 'musik2'
                }]
        };
    }
}

);