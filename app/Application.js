/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */
Ext.define('pertemuan.Application', {
    extend: 'Ext.app.Application',

    name: 'pertemuan',

    quickTips: false,
    platformConfig: {
        desktop: {
            quickTips: true
        }
    },

    stores: [
        // TODO: add global / shared stores here
        'pertemuan.store.Register',
    ],

    launch: function () {
        // TODO - Launch the application
        x = localStorage.getItem("LoggedIn");
        if (!x) {
            if(!Ext.getCmp('loginview')){
                    this.overlay = Ext.Viewport.add({
                        xtype: 'mylogin',
                        id: 'loginview',
                        floated: true,
                        //modal: true,
                        //hideOnMaskTap: true,
                        showAnimation: {
                            type: 'popIn',
                            duration: 250,
                            easing: 'ease-out'
                        },
                        hideAnimation: {
                            type: 'popOut',
                            duration: 250,
                            easing: 'ease-out'
                        },
                        //centered: true,
                        width: "100%",//Ext.filterPlatform('ie10') ? '100%' : (Ext.os.deviceType == 'Phone') ? 260 : 400,
                        height: "100%",
                        header: {
                            title: 'Login Form'
                        }
                    });
                    
            }
            Ext.getCmp('loginview').show();
        }

    },

    onAppUpdate: function () {
        Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
            function (choice) {
                if (choice === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});
